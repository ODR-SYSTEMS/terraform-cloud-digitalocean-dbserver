# Root SSH Key
resource "digitalocean_ssh_key" "root" {
  name       = "DBUSER Public Key"
  public_key = "${var.DBUSER_PUBLIC_KEY}"
}
resource "digitalocean_droplet" "bastiondb" {
  image = "centos-7-x64"
  name = "bastiondb"
  region = "nyc1"
  size = "s-1vcpu-2gb"
  ssh_keys = [
    digitalocean_ssh_key.root.fingerprint
  ]
  connection {
    type     = "ssh"
    user     = "root"
    private_key = "${var.DBUSER_PRIVATE_KEY}"
    timeout  = "2m"
    host = "${digitalocean_droplet.bastiondb.ipv4_address}"
  }
  provisioner "file" {
    source      = "progress_checker.sh"
    destination = "/tmp/progress_checker.sh"
  }
  provisioner "file" {
    source      = "nginx.conf"
    destination = "/tmp/nginx.conf"
  }
  provisioner "file" {
    source      = "my.cnf"
    destination = "/tmp/my.cnf"
  }
  provisioner "remote-exec" {
    inline = [
      "echo 'dbuser-backup ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers; adduser dbuser-backup; mkdir -p /home/dbuser-backup/.ssh; chmod 700 /home/dbuser-backup/.ssh; echo '${var.DBUSER_PUBLIC_KEY}' >> /home/dbuser-backup/.ssh/authorized_keys; chmod 600 /home/dbuser-backup/.ssh/authorized_keys; chown -R dbuser-backup:dbuser-backup /home/dbuser-backup",
      "echo \"-----BEGIN OPENSSH PRIVATE KEY-----\" > /home/dbuser-backup/.ssh/id_rsa",
      "echo '${var.DBUSER_PRIVATE_KEY}' >> /home/dbuser-backup/.ssh/id_rsa",
      "echo \"-----END OPENSSH PRIVATE KEY-----\" >> /home/dbuser-backup/.ssh/id_rsa",
      "chmod 0600 /home/dbuser-backup/.ssh/id_rsa; chown -R dbuser-backup:dbuser-backup /home/dbuser-backup",
      "echo 'ej ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers; adduser ej; mkdir -p /home/ej/.ssh; chmod 700 /home/ej/.ssh; echo '${var.EJ_PUBLIC_KEY}' >> /home/ej/.ssh/authorized_keys; chmod 600 /home/ej/.ssh/authorized_keys; chown -R ej:ej /home/ej",
      "mkdir -p /tmp/ssl",
      "echo \"-----BEGIN CERTIFICATE-----\" > /tmp/ssl/cert.pem",
      "echo '${var.NGINX_SSL_CERT}' >> /tmp/ssl/cert.pem",
      "echo \"-----END CERTIFICATE-----\" >> /tmp/ssl/cert.pem",
      "echo '-----BEGIN PRIVATE KEY-----' | sudo tee /tmp/ssl/key.pem > /dev/null",
      "echo '${var.NGINX_SSL_KEY}' >> /tmp/ssl/key.pem",
      "echo '-----END PRIVATE KEY-----' >> /tmp/ssl/key.pem",
      "mkdir /tmp/mysql_certs",
      "echo '-----BEGIN CERTIFICATE-----' > /tmp/mysql_certs/ca.pem",
      "echo '${var.MYSQL_CA}' >> /tmp/mysql_certs/ca.pem",
      "echo '-----END CERTIFICATE-----' >> /tmp/mysql_certs/ca.pem",
      "echo '-----BEGIN CERTIFICATE-----' > /tmp/mysql_certs/server-cert.pem",
      "echo '${var.MYSQL_SERVER_CERT}' >> /tmp/mysql_certs/server-cert.pem",
      "echo '-----END CERTIFICATE-----' >> /tmp/mysql_certs/server-cert.pem",
      "echo '-----BEGIN RSA PRIVATE KEY-----' > /tmp/mysql_certs/server-key.pem",
      "echo '${var.MYSQL_SERVER_KEY}' >> /tmp/mysql_certs/server-key.pem",
      "echo '-----END RSA PRIVATE KEY-----' >> /tmp/mysql_certs/server-key.pem",
      "chmod 0444 /tmp/mysql_certs/ca.pem && chmod 0400 /tmp/mysql_certs/server-key.pem && chmod 0444 /tmp/mysql_certs/server-cert.pem",
      "sh /tmp/progress_checker.sh",
    ]
  }
  user_data = data.template_file.cloud-init-yaml.rendered
}
data "cloudflare_zone" "advocatediablo_autodeploy" {
  name = "auto-deploy.net"
}
resource "cloudflare_record" "advocatediablo_mysql_dev" {
  zone_id   = data.cloudflare_zone.advocatediablo_autodeploy.id
  name    = "mysql-dev"
  value   = "${digitalocean_droplet.bastiondb.ipv4_address}"
  type    = "A"
  ttl     = 300
  proxied = false
}
data "template_file" "cloud-init-yaml" {
  template = file("${path.module}/cloud-init.yaml")
  vars = {
    DBUSER_SA_PASS = "${var.DBUSER_SA_PASS}",
    NGINX_SSL_CERT = "${var.NGINX_SSL_CERT}",
    NGINX_SSL_KEY = "${var.NGINX_SSL_KEY}",
    MYSQL_CA = "${var.MYSQL_CA}",
    MYSQL_SERVER_CERT = "${var.MYSQL_SERVER_CERT}",
    MYSQL_SERVER_KEY = "${var.MYSQL_SERVER_KEY}",
    MY_GIT_PRV_KEY = "${var.DBUSER_PRIVATE_KEY}"
  }
}
